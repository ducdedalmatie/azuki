const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("Azuki", function () {
  it("Should return the new greeting once it's changed", async function () {
    const signers = await ethers.getSigners();
    const [ deployer ] = signers;
    const Azuki = await ethers.getContractFactory("Azuki");
    const azuki = await Azuki.deploy(5, 10000, 8900, 200);
    await azuki.deployed();

    // Set baseURI
    const baseURI = 'https://azuki-prereveal.s3-us-west-1.amazonaws.com/metadata/';
    const baseURItx = await azuki.setBaseURI(baseURI);

    // await baseURItx.wait();

    const auctionSaleStartTime = 1642010400;
    console.log('Auction Sale', new Date(auctionSaleStartTime * 1000).toISOString());
    const aucStartTimeTx = await azuki.setAuctionSaleStartTime(auctionSaleStartTime);

    console.log(await azuki.balanceOf(deployer.address));

    // mint for developers
    const devMintTx = await azuki.devMint(200);
    await devMintTx.wait();

    // seed allowlist
    const allowlist = signers.filter((sign, index) => index === 0 ? false : true)
      .map(sign => sign.address);
    const slots = [ 1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3, 1 ];
    const seedAllowlistTx = await azuki.seedAllowlist(allowlist, slots);
    const slot = await azuki.allowlist(allowlist[5]);
    expect(slot).to.equal(slots[5]);

    // endAuctionAndSetupNonAuctionSaleInfo
    const mintlistPriceWei = '500000000000000000';
    const publicPriceWei = '1000000000000000000';
    const publicSaleStartTime = 1642269600;
    console.log('Public Sale', new Date(publicSaleStartTime * 1000).toISOString())

    const nonAuctionInfoTx = await azuki.endAuctionAndSetupNonAuctionSaleInfo(
      mintlistPriceWei,
      publicPriceWei,
      publicSaleStartTime
    );
    
    // endAuctionAndSetupNonAuctionSaleInfo 2
    const mintlistPriceWei2 = '500000000000000000';
    const publicPriceWei2 = 0;
    const publicSaleStartTime2 = 1674145770
    console.log('Public Sale 2', new Date(publicSaleStartTime2 * 1000).toISOString())

    const nonAuctionInfo2Tx = await azuki.endAuctionAndSetupNonAuctionSaleInfo(
      mintlistPriceWei2,
      publicPriceWei2,
      publicSaleStartTime2
    );

    // change BaseURI
    const revealBaseURI = 'https://ikzttp.mypinata.cloud/ipfs/QmeBWSnYPEnUimvpPfNHuvgcK9wFH9Sa6cZ4KDfgkfJJis/';
    const revealBaseURItx = await azuki.setBaseURI(revealBaseURI);

    console.log('Token:', (await azuki.balanceOf(azuki.address)).toNumber());
    console.log('Deployer:', (await azuki.balanceOf(deployer.address)).toNumber());


  });
});
